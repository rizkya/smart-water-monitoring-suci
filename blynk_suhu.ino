#include <Ultrasonic.h> //memanggil liblary ultrasonic
#include <ESP8266WiFi.h> //memanggil liblary ESP8266WiFi
#include <BlynkSimpleEsp8266.h> //memanggil liblary Blynk
BlynkTimer timer; //inisialisasi timer di blynk

#define BLYNK_PRINT Serial

/* Fill in information from Blynk Device Info here */
#define BLYNK_TEMPLATE_ID "TMPL6UBzOzdac"  //isi dengan template id yang ada di blynk
#define BLYNK_TEMPLATE_NAME "SmartWaterMonitoring" //isi dengan template name yang ada di blynk
#define BLYNK_AUTH_TOKEN "EjYC3-4_FNftNmXhJKqqzlegkwundRNV" //isi dengan blynk auth yang ada di blynk



// Your WiFi credentials.
// Set password to "" for open networks.
char ssid[] = "tambahan";  //isi dengan nama wifi yang mempunyai koneksi internet
char pass[] = "tambahan"; //isi password dari wifi tersebut

Ultrasonic ultrasonic(14, 12); //inisialisasi pin ultrasonic 12 untuk echo dan 13 untuk trigger
int distance; //inisialsisasi nama distance

const int THERMISTOR_PIN = A0; //untuk sensor suhu yang berada di Pin A0
const int THERMISTOR_PULLUP_RESISTOR = 950000; // isi dengan nilai resistor pullup yang terhubung dengan sensor suhu

// Constants for thermistor resistance at 25 degrees Celsius
const double THERMISTOR_REF_RESISTANCE = 8052; // 10k
const double THERMISTOR_TEMP_R25 = 30; // 25 degrees Celsius
double suhu = 0;

int tinggi_tank = 100; //nilai tinggi tank, nilai 100 dalam satuan cm
int val_min = 5;



void setup()
{
  // Debug console
  Serial.begin(115200); //baut rate serial monitor di nilai 115200
  pinMode(LED_BUILTIN, OUTPUT); //pin led sebagai output
  Blynk.begin(BLYNK_AUTH_TOKEN, ssid, pass); //autentifikasi dengan server blynk
  while (Blynk.connect() == false) {  }
  digitalWrite(LED_BUILTIN, LOW); //led indikator mati
  timer.setInterval(1000L, reconnectBlynk);
  timer.setInterval(1000L, send_temp); // jeda antar pengiriman data pada server blynk, 1000L merupakan nilai dalam ms atau milidetik
  timer.setInterval(1000L, baca_jarak); // jeda antar pengiriman data pada server blynk, 1500L merupakan nilai dalam ms atau milidetik
}

void loop()
{
  timer.run(); //menjalankan function internal timer
  if (Blynk.connected()) { //ketika terkoneksi dengan server blynk
    Blynk.run(); //menjalankan function internal blynk
  }
}

void send_temp() { //function untuk mengambil dan mengirim data
  int rawADC = analogRead(THERMISTOR_PIN); //membaca nilai analog sensor yang berada pada A0 atau Thermistor pin
  double voltage = rawADC * (3.3 / 1023.0); // 3.3V reference voltage on ESP8266

  // Calculate thermistor resistance using voltage divider formula
  double thermistorResistance = (THERMISTOR_PULLUP_RESISTOR * voltage) / (3.3 - voltage);

  // Calculate temperature using Steinhart-Hart equation
  double steinhart = thermistorResistance / THERMISTOR_REF_RESISTANCE; // (R/R25)
  steinhart = log(steinhart);
  steinhart /= 3950; // Thermistor's beta coefficient
  steinhart += 1.0 / (THERMISTOR_TEMP_R25 + 273.15); // +273.15 to convert to Kelvin
  steinhart = 1.0 / steinhart; // Invert
  suhu = steinhart - (273.15 - 68); // Convert to Celsius


  //menampilkan nilai temperature yang dibaca esp8266
  Serial.print("Temperature: ");
  Serial.print(suhu);
  Serial.println(" °C");
  // sampai sini untuk menampilkan nya
  Blynk.virtualWrite(V2, suhu);  //mengirimkan data pada pin Virtual 2 atau V2 pada server blynk

}

void baca_jarak() {
  distance = ultrasonic.read();
  if (distance >= tinggi_tank) {
    distance = tinggi_tank;
  }
  if (distance < val_min) {
    distance = val_min;
  }
  Serial.print("Distance in CM: ");
  Serial.println(distance);
  int persen_air = map(distance, tinggi_tank, val_min, 0, 100); //mengubah nilai jarak menjadi satuan persen
  Blynk.virtualWrite(V1, persen_air); //mengirimkan nilai persen air pada server blynk V1 atau pin Virtual 1
}

void reconnectBlynk() { //function untuk melakukan koneksi ulang ketika tidak terkoneksi dengan server blynk
  if (!Blynk.connected()) {
    Serial.println("Koneksi Terputus");
    if (Blynk.connect()) { //ketika koneksi terhubung dengan server
      Serial.println("Koneksi Terhubung");
      digitalWrite(LED_BUILTIN, HIGH);  //led indikator menyala
    }
    else {
      Serial.println("Belum terhubung dengan server blynk");
      digitalWrite(LED_BUILTIN, LOW); //led indikator mati
    }
  }
}
